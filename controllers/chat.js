import { Configuration, OpenAIApi } from "openai";

export default async function Chat(req, res) {
  const configuration = new Configuration({
    apiKey: process.env.OPENAI_API_KEY,
    organization: process.env.ORGANIZATION_ID
  });

  const openai = new OpenAIApi(configuration);

  const { message } = req.body;
  console.log(message);

  const prompt = `You are a helpful assistant.\nUser: ${message}\nAssistant:`;
  const response = await openai.createCompletion({
    model: "gpt-3.5-turbo",
    prompt,
    temperature: 0.8,
    maxTokens: 150,
  });

  const reply = response.data.choices[0]
  console.log(reply);

  res.status(200).json({ reply });
}